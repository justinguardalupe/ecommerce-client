import React from 'react';
import { Redirect } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Banner from '../components/Banner';



export default function Error(){
	const data = {
		title: "404 - Page Not Found",
		content: "The page you're looking for is not found.",
		destination: "/",
		label: "Back home"
	}

	return(
		<Banner data={data} />
	)
}
