import React, { Fragment, useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import AppNavbar from '../components/AppNavbar';
import CartView from './CartView';



export default function ProductView(props) {
	
	const userId = localStorage.getItem('id');
	const productId = props.match.params.productId;

	const [name, setName] = useState([]);
	const [description, setDescription] = useState([]);
	const [price, setPrice] = useState([]);

	useEffect(() => {
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				}
			);
	}, [])
	

	const addToCart = async () => {
        fetch(`http://localhost:4000/carts/${userId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                customerId: userId,
				productId: productId,
				price: price,
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
   	    })
	}

	return(
		<Fragment>
		<AppNavbar/>
		<Row className="spacer"></Row>
		
		<Container className="mt-5">
				<Card>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{price}</Card.Text>					
						<Link className="btn btn-success" onClick={addToCart} to={`/carts/${userId}`}>Add to Cart</Link>
					</Card.Body>
				</Card>
		</Container>
		</Fragment>
	)
}
