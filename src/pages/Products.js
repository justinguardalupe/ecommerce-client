import React from 'react';
import { Fragment, useEffect, useState } from 'react';
import { Container, Card, Button, Row, Col, CardGroup } from 'react-bootstrap';

import ProductCard from '../components/ProductCard';
import AppNavbar from '../components/AppNavbar';



export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/products/')
		.then(res => res.json())
		.then(data => {

			setProducts(data.map(product => {
					return(
						<ProductCard key={product.id} productProp={product} />
					)
				})
			);
			
		})
	}, [])

	return(
		<Fragment>
			<AppNavbar />

			<h3 className="mt-5">Bestselling Products</h3>
			<CardGroup>{products}</CardGroup>

			<h3 className="mt-5">Recommended For You</h3>
			<CardGroup>{products}</CardGroup>
		</Fragment>
	)
}
