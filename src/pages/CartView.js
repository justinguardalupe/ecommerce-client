import React, { Fragment } from 'react';
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardGroup } from 'react-bootstrap';

import AppNavbar from '../components/AppNavbar';
// import CartCheckOut from './CartCheckOut';
import CartCard from '../components/CartCard';



export default function CartView() {
	
	const [cart, setCart] = useState([]);
	const [cartItems, setCartItems] = useState([]);

	const userId = localStorage.getItem('id');

	useEffect(() => {
		fetch(`http://localhost:4000/carts/${userId}`)
		.then(res => res.json())
		.then(data => {

			data.map(cart => {
				setCart({cart});		
			})
			console.log(cart);
			setCartItems(cart.items.map(cartItem => {
				console.log(cartItems);
				return(
					<CartCard key={cartItem._id} cartProp={cartItem} />
				)
				}
			))
			
		})
	}, [])

	const checkOut = async () => {

	}

	return(
	<Fragment>
		<AppNavbar/>
		<Row className="spacer"></Row>

		<Card>{cart}</Card>
	</Fragment>
	)
}
