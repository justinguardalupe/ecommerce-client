import React, { useState } from "react";
import Table from "../Components/Table";

const OrderHistory = ({ orders }) => {
  return <Table data={orders} />;
};

export default OrderHistory;
