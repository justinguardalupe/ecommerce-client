/* Packages */
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col, Image } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

/* Pages */
import UserContext from '../UserContext';
// import image from "../assets/fruits-stand.png";



export default function Login() {
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);    

	async function authenticate(e) {
	    e.preventDefault();

        fetch('http://localhost:3000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login successful",
                    icon: "success",
                    text: "Welcome to Sun Center!",
                    confirmButtonColor: '#008000',
                })           
            }
            else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again.",
                    confirmButtonColor: '#008000',
                })                  
            }
        })
	    setEmail('');
	    setPassword('');
	}

    const retrieveUserDetails = async (token) => {
        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

	useEffect(() => {
        if(email !== '' && password !== ''){setIsActive(true);}
        else{setIsActive(false);}}, 
        [email, password] );	

    localStorage.setItem('id', user.id);
    localStorage.setItem('isAdmin', user.isAdmin);


return (
(user.id !== null) ?
    (user.isAdmin === true) ? 
        <Redirect to="/admin" />
        : 
        <Redirect to="/home" />
    :
    <Container className="auth-page">
        <Row className="d-flex align-items-center">
            <Col xs={12} lg={6} className="left-column">
                <div className="widget-auth">
                    <div className="d-flex align-items-center justify-content-between py-3">
                        <p className="auth-header mb-0">Sign In</p>
                        <Link className="logo-block" to={`/home`} >
                        </Link>
                    </div>

                <Form onSubmit={(e) => authenticate(e)}>
                    <Form.Group controlId="userEmail" className="mt-3">
                        <Form.Text>Email address</Form.Text>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email"
                            className="input-transparent pl-3" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password" className="mt-3">
                        <Form.Text>Password</Form.Text>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            className="input-transparent pl-3"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>
                    { isActive ? 
                        <div className="bg-widget d-flex justify-content-center">
                            <Button className="rounded-pill my-3 bg-success" type="submit" id="submitBtn">
                                <b>Submit</b>
                            </Button>
                        </div>
                        : 
                        <div className="bg-widget d-flex justify-content-center">
                            <Button className="rounded-pill my-3 bg-danger" type="submit" id="submitBtn" disabled>
                                <b>Submit</b>
                            </Button>
                        </div>
                    }
                </Form>
                </div>
            </Col>
        </Row>
    </Container>
    )
}
