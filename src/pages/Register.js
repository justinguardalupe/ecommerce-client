/* Packages */
import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Button, FormGroup, FormText, Input } from "react-bootstrap";
import { Redirect, useHistory, Link } from 'react-router-dom';
import { Form, Image } from 'react-bootstrap';
import Swal from 'sweetalert2';

/* Pages */
import Home from "./Home";
import UserContext from '../UserContext';

export default function Register(){
    const {user, setUser} = useContext(UserContext);
    const history = useHistory();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobile, setMobile] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	async function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
            	Swal.fire({
                	title: "Duplicate email found",
                	icon: "error",
                	text: "Please provide a different email.",
                	confirmButtonColor: '#008000',
            	})
            }
            else {
				fetch('http://localhost:4000/users/register', {
			        method: 'POST',
			        headers: {
			            'Content-Type': 'application/json; charset=UTF-8'
			        },
			        body: JSON.stringify({
			            email: email,
			            password: password1,
			            firstName: firstName,
						lastName: lastName,
						mobile: mobile
			        })
			    })
			    .then(res => res.json())
			    .then(data => {
			        if(data === true){
			            Swal.fire({
			                title: "Registration successful",
			                icon: "success",
			                text: "Hello fellow Burgler",
			    			confirmButtonColor: '#008000',
			            });  
			    		history.push("/login");
			        }
			        else {
			            Swal.fire({
			                title: "Registration failed",
			                icon: "error",
			                text: "Check details and try again.",
			                confirmButtonColor: '#008000',
			            })
			        }
			    })
			    setFirstName('');
			    setLastName('');
				setEmail('');
				setMobile('');
				setPassword1('');
				setPassword2('');
		}
	})				

}


	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobile.length === 11) )
				{setIsActive(true);}
		else {setIsActive(false); }}, 
		[firstName, lastName, mobile, email, password1, password2] );


return(

(user.id !== null) 
	?
	<Redirect to="/login" />
	:
    <Container className="auth-page">
        <Row className="d-flex align-items-center">
          <Col md={12} lg={6} className="left-column">
          	<div className="widget-auth">
          	<div className="d-flex align-items-center justify-content-between py-3">
                <p className="auth-header mb-0">Sign Up</p>
                <Link className="logo-block" to={`/home`} >
                  	<p className="mb-0">SUN CENTER</p>
                </Link>
            </div>

			<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName" className="mt-3">
			  	<Form.Text>First Name</Form.Text>
			  	<Form.Control 
			  	type="firstName" 
			  	placeholder="Enter first name" 
			  	className="input-transparent pl-3"
			  	value={firstName}
			  	onChange = { e => setFirstName(e.target.value) }
			  	required />
			</Form.Group>

			<Form.Group controlId="lastName" className="mt-3">
			  	<Form.Text>Last Name</Form.Text>
			  	<Form.Control 
			  	type="lastName" 
			  	placeholder="Enter last name"
			  	className="input-transparent pl-3" 
			  	value={lastName}
			  	onChange = { e => setLastName(e.target.value) }
			  	required />
			</Form.Group>

		  	<Form.Group controlId="userEmail" className="mt-3">
		    	<Form.Text>Email address</Form.Text>
		    	<Form.Control 
		    	type="email" 
		    	placeholder="Enter email"
		    	className="input-transparent pl-3" 
		    	value={email}
		    	onChange = { e => setEmail(e.target.value) }
		    	required />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>   	
		  	</Form.Group>

		  	<Form.Group controlId="mobile" className="mt-3">
			  	<Form.Text>Mobile Number</Form.Text>
			  	<Form.Control 
			  	type="mobile" 
			  	placeholder="Enter mobile number"
			  	className="input-transparent pl-3" 
			  	value={mobile}
			  	onChange = { e => setMobile(e.target.value) }
			  	required />
			</Form.Group>

		  	<Form.Group controlId="password1" className="mt-3">
		    <Form.Text>Password</Form.Text>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Enter password" 
		    	className="input-transparent pl-3"
		    	value={password1}
		    	onChange = { e => setPassword1(e.target.value) }
		    	required />
		  	</Form.Group>
		  		
		  	<Form.Group controlId="password2" className="mt-3">
		  	<Form.Text>Verify Password</Form.Text>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Re-enter password"
		    	className="input-transparent pl-3" 
		    	value={password2}
		    	onChange = { e => setPassword2(e.target.value) }
		    	required />
		  	</Form.Group>
		  		
		  	{ isActive ? 
		  		<div className="bg-widget d-flex justify-content-center">
		  			<Button className="rounded-pill my-3" color="danger" type="submit" id="submitBtn">
		    			<b>Submit</b>
		  			</Button>
		  		</div>
		  		: 
		  		<div className="bg-widget d-flex justify-content-center">
		  			<Button className="rounded-pill my-3" type="submit" id="submitBtn" color="success" disabled>
		    			<b>Submit</b>
		  			</Button>
		  		</div>
			}
			</Form>

			</div>
			</Col>

		</Row>
	</Container>

	)
}
