import React, { Fragment } from 'react';
import { Row } from 'react-bootstrap';
import Banner from '../components/Banner';

import AppNavbar from '../components/AppNavbar';
import user from '../App';

export default function Home(props){

	const data = {
		title: "Welcome to Burgler's",
		title2: "Home to 5-star rated cheap burgers",
		content: "It's a steal!",
		destination: "/",
		label: "Back home"
	}

	return(
		<Fragment>
			<AppNavbar/>
			<Row className="spacer"></Row>
			<Banner data={data} />
		</Fragment>		
	)
}
