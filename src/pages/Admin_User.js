/* Packages */
import React, { Fragment, useEffect, useState, useContext } from 'react';
import { Nav, Container, Card, Button, Row, Col, Image, CardGroup } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

/* Pages */
import ProductCard from '../components/ProductCard';
// import image from "../assets/fruits-stand.png";
import AppNavbar from '../components/AppNavbar';
import UserContext from '../UserContext';
import user from '../App';



export default function Admin(){

	let isAdmin;

	isAdmin = localStorage.getItem(isAdmin);

	const [ allProducts, setAllProducts ] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/products/all')
		.then(res => res.json())
		.then(data => {

			setAllProducts(data.map(product => {
					return(
						<ProductCard key={product.id} productProp={product} />
					)
				})
			);
			
		})
	}, [])

	return(
		<Fragment>
		<AppNavbar/>
		<Row className="spacer"></Row>

		<Row className="mt-5">
		<Col xs={0} md={2} className="border-1">
		
			<Nav justify variant="pills" defaultActiveKey="/admin" className="flex-column bg-light text-light">
			  <Nav.Item>
			    <Nav.Link to={`/admin`}>Products</Nav.Link>
			  </Nav.Item>
			  <Nav.Item>
			    <Nav.Link to={`/admin/users`} eventKey="link-1">Users</Nav.Link>
			  </Nav.Item>
			  <Nav.Item>
			    <Nav.Link to={`/admin/orders`} eventKey="link-2">Orders</Nav.Link>
			  </Nav.Item>
			</Nav>

		</Col>

		<Col xs={12} md={10}>
			<Card>{allProducts}</Card>
		</Col>
		</Row>
		</Fragment>
	)
}
