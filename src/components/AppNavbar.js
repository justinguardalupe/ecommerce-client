import React from 'react';
import { Fragment, useContext } from 'react';
import { Nav, Navbar, Container, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { Icon } from '@iconify/react';
import UserContext from '../UserContext';



export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="dark" expand="lg" fixed="top" variant="dark" className="p-3">
		  <Container fluid>
		  <Icon icon="mdi:hamburger" height="3em" className="logocolor" />
		    <Navbar.Brand class="navbrand ml-2 mr-3">Burgler's</Navbar.Brand>
		    <Navbar.Toggle aria-controls="navbarScroll" />
		    <Navbar.Collapse id="navbarScroll">
		      <Nav
		        className="me-auto my-3 my-lg-0"
		        style={{ maxHeight: '150' }}
		        navbarScroll
		      >
		        <Nav.Link href="/home">Home</Nav.Link>
		        <Nav.Link href="/products">Products</Nav.Link>
		        {(user.isAdmin === true) ? 
		        	<Nav.Link href="/carts/${user.id}" disabled>Cart</Nav.Link>
		        	:
		        	<Nav.Link href="/carts/${user.id}">Cart</Nav.Link>
		        }
		        
		      </Nav>

		      	{(user.id !== null) ?
		      		(user.isAdmin === true) ?
		      			<Fragment>
		      				<Nav class="d-flex" bg="dark" variant="dark">
		      				<Nav.Link as={NavLink} to="/admin" exact>Admin</Nav.Link>
		    				<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		    				</Nav>
		    			</Fragment>
		    			:
		      			<Nav class="d-flex" bg="dark" variant="dark">
		    				<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		    			</Nav>
		    		:
		    		<Fragment class="d-flex">
		    			<Nav bg="dark" variant="dark">
		    			<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
		    			<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		    			</Nav>
		    		</Fragment>
		    	}

		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}

