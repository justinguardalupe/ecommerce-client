import React from "react";
import TableRow from "./TableRow";



const Table = function (props) {
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th>Price</th>
        </tr>
      </thead>

      <tbody>
        {props.map(function (item, index) {
          return (
            <TableRow
              key={item.id}
              title={item.title}
              description={item.description}
              price={item.price}
            />
          );
        })}
      </tbody>
    </table>
  );
};

export default Table;
