import React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// import image from '../assets/bg.svg';
import ProductView from '../pages/ProductView';

export default function ProductCard({productProp}){

	const {_id, name, description, price} = productProp;
	const [ product, setProduct] = useState([]);

	const getDetails = () => {
		let id = this._id;
		let name = this.name;
		let description = this.description;
		let price = this.price;
		setProduct(id, name, description, price);

		ProductView(product);
	}


	return(
		<Card className="m-3">
		  <Card.Body>
		  	<Card.Img variant="top" fluid className="p-2" height={130}/>
		    <Card.Title>{name}</Card.Title>
		    <Card.Text>{description}</Card.Text>
		    <Card.Text>₱ {price}</Card.Text>
		    <Link className="btn btn-success" onPress={getDetails} to={`/products/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>
	)
}


ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
	})

}
