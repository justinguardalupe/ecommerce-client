import React from "react";

function TableRow(props) {
  return (
    <tr>
      <td>{props.title}</td>
      <td>{props.description}</td>
      <td>{props.price}</td>
    </tr>
  );
}

export default TableRow;