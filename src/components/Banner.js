import React from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';

export default function Banner({data}){
	console.log(data);

	const {title, title2, content, destination, label} = data;

		return(
			<Row>
				<Col className="p-5">	
 					<h1 className="Banner1">{title}</h1>
 					<h2 className="Banner2">{title2}</h2>
 					<p>{content}</p>
 					<Link to={destination}>
 						<Button variant="primary" to="/">{label}</Button>
 					</Link>
 				</Col>
 			</Row>

		)

}

