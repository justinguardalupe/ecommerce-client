/* Packages */
import React, { useState, useEffect} from 'react';
import { BrowserRouter as Router, Route, Switch, useHistory } from 'react-router-dom';
import { Container } from 'react-bootstrap';

/* Pages */
import ProductView from './pages/ProductView';
import Home from './pages/Home';
import Error from './pages/Error';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import CartView from './pages/CartView';
// import CartCheckOut from './pages/CartCheckOut';
import Admin from './pages/Admin';
import { UserProvider } from './UserContext';

/* Style */
import './App.css';
//import './styles/styles.scss'


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/home" component={Home} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/products/:productId" component={ProductView} />
            <Route exact path="/carts/${user.id}" component={CartView} />

            <Route exact path="/admin" component={Admin} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/register" component={Register} />
            <Route path='/*' component={Error} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>    
  );
}

export default App;
//            <Route exact path="/orders/${user.id}" component={CartCheckOut} />